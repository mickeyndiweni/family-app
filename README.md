# family-app

A RESTful service with basic CRUD endpoints for creating and retrieving  Parent and Child entity data from an in memory H2 database. To get started use the /parents POST endpoint to store data in the database, as no data is persisted beyond application runtime.

