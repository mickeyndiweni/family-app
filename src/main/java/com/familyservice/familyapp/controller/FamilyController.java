package com.familyservice.familyapp.controller;

import com.familyservice.familyapp.model.Parent;
import com.familyservice.familyapp.service.ParentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping( value = "/parents")
public class FamilyController {


    @Autowired
    private ParentService parentService;

    @GetMapping( value =  "/{id}" )
    public Parent getParent( @PathVariable final String id ) {
        return parentService.getParent( Integer.parseInt( id ) );
    }

    @PostMapping
    public Parent createParent( @RequestBody final Parent parent ) {
        parentService.createParent( parent );
        return parentService.getParent( parent.getId() );
    }

    @GetMapping( produces = MediaType.APPLICATION_JSON_VALUE )
    public Collection< Parent > getAllParents() {
        return parentService.getAllParents();
    }

    @GetMapping( "/test" )
    public String testRun() {
        return "test-success";
    }
}
