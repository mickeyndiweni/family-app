package com.familyservice.familyapp.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Parent extends Person {

    private Integer id;
    private String title;
    private List< Child > children;

    public Parent() {
    }

    @Id
    @GeneratedValue
    public Integer getId() {
        return id;
    }

    public void setId( Integer id) {
        this.id = id;
    }

    @Column
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @OneToMany( mappedBy = "parent",
                fetch = FetchType.LAZY )
    public List< Child > getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }
}
