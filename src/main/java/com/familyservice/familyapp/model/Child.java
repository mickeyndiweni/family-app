package com.familyservice.familyapp.model;

import javax.persistence.*;

@Entity
public class Child extends Person {

    private Long id;
    private Parent parent;

    public Child() {
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "parent_child_no",
                 insertable = false,
                 updatable = false )
    public Parent getParent() {
        return parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }
}
