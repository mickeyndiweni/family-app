package com.familyservice.familyapp.service;

import com.familyservice.familyapp.model.Parent;

import java.util.Collection;

public interface ParentService< T extends Parent> {

    T getParent( Integer id );

    void createParent( T parent );

    Collection< T > getAllParents();


}
