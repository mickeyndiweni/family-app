package com.familyservice.familyapp.service.impl;

import com.familyservice.familyapp.model.Parent;
import com.familyservice.familyapp.repository.ParentRepository;
import com.familyservice.familyapp.service.ParentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ParentServiceImpl implements ParentService {


    private ParentRepository parentRepository;

    @Autowired
    public ParentServiceImpl( ParentRepository personRepository ) {
        this.parentRepository = personRepository;
    }

    @Override
    public Parent getParent( Integer id ) {
        return parentRepository.findById( id );
    }

    @Override
    public void createParent( Parent parent ) {
        parentRepository.save( parent );
    }

    @Override
    public Collection< Parent > getAllParents() {
        return parentRepository.findAll();
    }
}
