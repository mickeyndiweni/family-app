package com.familyservice.familyapp.exception;

public class NoSuchDataException extends Exception {


    public NoSuchDataException() {
    }

    public NoSuchDataException( String message ) {
        super( message );
    }

    public NoSuchDataException( String message, Throwable cause ) {
        super( message, cause );
    }

    public NoSuchDataException( Throwable cause ) {
        super( cause );
    }

    public NoSuchDataException( String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace ) {
        super( message, cause, enableSuppression, writableStackTrace );
    }
}
