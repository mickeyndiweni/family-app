package com.familyservice.familyapp.repository;

import com.familyservice.familyapp.model.Parent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParentRepository extends JpaRepository< Parent, Long > {

    Parent findById( Integer id );

    Parent save( Parent parent );

}
