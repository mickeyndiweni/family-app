package com.familyservice.familyapp.repository;

import com.familyservice.familyapp.exception.NoSuchDataException;
import com.familyservice.familyapp.model.Parent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith( SpringRunner.class )
@DataJpaTest
public class ParentRepositoryTest {

    @Autowired
    private ParentRepository parentRepository;


    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findById() throws Exception {
        Parent parent = parentRepository.findById( 1 );
        if ( parent == null ) {
            throw new NoSuchDataException( "Record not found!" );
        }

        //assertThat( ) ;
    }

    @Test
    public void save() {
    }
}