package com.familyservice.familyapp.controller;

import com.familyservice.familyapp.model.Parent;
import com.familyservice.familyapp.service.ParentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringBootTest
@AutoConfigureMockMvc
public class FamilyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private ParentService parentService;

    private Parent parent1, parent2;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        setUpParent1();
        setUpParent2();
        mockMvc.perform( MockMvcRequestBuilders.post( "/parents" )
                .contentType( MediaType.APPLICATION_JSON_UTF8 )
                .content( TestUtil.convertObjectToJsonBytes(parent1) ) );
        mockMvc.perform( MockMvcRequestBuilders.post( "/parents" )
                .contentType( MediaType.APPLICATION_JSON_UTF8 )
                .content( TestUtil.convertObjectToJsonBytes(parent2) ) );
    }

    @Test
    public void getParent_withAnyIDParameter_returns200ResponseStatus() throws Exception {

        mockMvc.perform( MockMvcRequestBuilders.get( "/parents/{id}", 12345 )
                .accept( MediaType.APPLICATION_JSON_UTF8 ) )
                .andExpect( status().isOk() );
    }

    @Test
    public void getParent_withIDParameter_1_returnsJaneDoe() throws Exception {

        mockMvc.perform( MockMvcRequestBuilders.get( "/parents/{id}", 1 ) )
                .andExpect( content().contentType( TestUtil.APPLICATION_JSON_UTF8 ) )
                .andExpect( jsonPath( "$.title", is( "Mrs" ) ) )
                .andExpect( jsonPath( "$.firstName", is( "Jane" ) ) )
                .andExpect( jsonPath( "$.lastName", is( "Doe" ) ) );

    }

    //@Test
    public void getParent_wrongIdUsed_returns404Response() throws Exception {
        mockMvc.perform( MockMvcRequestBuilders.get( "/parents/{id}", 10001 ) )
                .andExpect( status().isNotFound() );

    }

    @Test
    public void createParent_newParentEntry_ShouldAddNewParentEntryAndReturnOkResponse() throws Exception {
        Parent parent3 = new Parent();
        parent3.setTitle( "Sir" );
        parent3.setFirstName( "Jonny" );
        parent3.setLastName( "Smith" );
        mockMvc.perform( MockMvcRequestBuilders.post( "/parents" )
                                               .contentType( MediaType.APPLICATION_JSON_UTF8 )
                                               .content( TestUtil.convertObjectToJsonBytes( parent3 ) ) )
                .andExpect( status().isOk() )
                .andExpect( jsonPath("$.title", is( "Sir") ) )
                .andExpect( jsonPath("$.firstName", is( "Jonny" ) ) )
                .andExpect( jsonPath( "$.lastName", is( "Smith" ) ) );

    }

    @Test
    public void getAllParents() throws Exception {
        mockMvc.perform( MockMvcRequestBuilders.get( "/parents" ) )
                .andExpect( status().isOk() );
    }


    @Test
    public void testRun_returnsTestSuccessString() throws Exception  {
        mockMvc.perform( MockMvcRequestBuilders.get( "/parents/test" ) )
                .andExpect( status().isOk() )
                .andExpect( content().string( "test-success" ) );
    }

    private void setUpParent1() {
        parent1 = new Parent();
        parent1.setTitle( "Mrs");
        parent1.setFirstName( "Jane" );
        parent1.setLastName( "Doe" );

     }

    private void setUpParent2() {
        parent2 = new Parent();
        parent2.setTitle( "Mr");
        parent2.setFirstName( "Tim" );
        parent2.setLastName( "Story" );

    }
}